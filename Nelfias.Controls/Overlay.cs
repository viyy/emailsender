﻿using System.Windows;
using System.Windows.Controls;

namespace Nelfias.Controls
{
    /// <summary>
    ///     Выполните шаги 1a или 1b, а затем 2, чтобы использовать этот настраиваемый элемент управления в файле XAML.
    ///     Шаг 1a. Использование настраиваемого элемента управления в файле XAML, существующем в текущем проекте.
    ///     Добавьте атрибут XmlNamespace к корневому элементу файла разметки, где он
    ///     должен использоваться:
    ///     xmlns:MyNamespace="clr-namespace:Nelfias.Controls"
    ///     Шаг 1b. Использование этого настраиваемого элемента управления в файле XAML, существующем в текущем проекте.
    ///     Добавьте атрибут XmlNamespace к корневому элементу файла разметки, где он
    ///     должен использоваться:
    ///     xmlns:MyNamespace="clr-namespace:Nelfias.Controls;assembly=Nelfias.Controls"
    ///     Потребуется также добавить ссылку на проект из проекта, в котором находится файл XAML
    ///     в данный проект и пересобрать во избежание ошибок компиляции:
    ///     Правой кнопкой мыши щелкните проект в обозревателе решений и выберите команду
    ///     "Добавить ссылку"->"Проекты"->[Выберите этот проект]
    ///     Шаг 2)
    ///     Продолжайте дальше и используйте элемент управления в файле XAML.
    ///     <MyNamespace:CustomControl1 />
    /// </summary>
    public class Overlay : ContentControl
    {
        static Overlay()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Overlay), new FrameworkPropertyMetadata(typeof(Overlay)));
        }
    }
}