﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Nelfias.Crypto.Tests
{
    [TestClass]
    public class EncryptTests
    {
        private static readonly string PassPhrase = Base64.Decode("Z0IybmpuVjgyZlFUS0ZqaA==");

        [TestMethod]
        public void DecryptStringTest()
        {
            var input = "O9VV9Iwsmz4rnql7yI9onQ==";
            var expected = "11235813";
            var res = Encrypt.DecryptString(input, PassPhrase);
            Assert.AreEqual(expected, res);
        }

        [TestMethod]
        public void DecryptString_nullTest()
        {
            string input = null;
            Assert.ThrowsException<ArgumentNullException>(() => Encrypt.DecryptString(input, PassPhrase));
        }

        [TestMethod]
        public void DecryptString_RandomStringTest()
        {
            var input = "Just Another Random String (not Base64)";
            Assert.ThrowsException<FormatException>(() => Encrypt.DecryptString(input, PassPhrase));
        }

        [TestMethod]
        public void EncryptStringTest()
        {
            var expected = "O9VV9Iwsmz4rnql7yI9onQ==";
            var input = "11235813";
            var res = Encrypt.EncryptString(input, PassPhrase);
            Assert.AreEqual(expected, res);
        }

        [TestMethod]
        public void EncryptString_InversableTest()
        {
            var input = "RandomUserPassword";
            var res = Encrypt.DecryptString(Encrypt.EncryptString(input, PassPhrase), PassPhrase);
            Assert.AreEqual(input, res);
        }

        [TestMethod]
        public void EncryptString_nullTest()
        {
            string input = null;
            Assert.ThrowsException<ArgumentNullException>(() => Encrypt.EncryptString(input, PassPhrase));
        }
    }
}