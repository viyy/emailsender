﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Nelfias.Crypto.Tests
{
    [TestClass]
    public class Base64Tests
    {
        [TestMethod]
        public void EncodeTest()
        {
            var input = @"MyRandomString With<Some>'Unsafe'$Chars@";
            var expected = @"TXlSYW5kb21TdHJpbmcgV2l0aDxTb21lPidVbnNhZmUnJENoYXJzQA==";
            var res = Base64.Encode(input);
            Assert.AreEqual(expected, res);
        }

        [TestMethod]
        public void Encode_InversableTest()
        {
            var input = @"MyRandomString With<Some>'Unsafe'$Chars@";
            var res = Base64.Decode(Base64.Encode(input));
            Assert.AreEqual(input, res);
        }

        [TestMethod]
        public void Encode_nullTest()
        {
            string input = null;
            Assert.ThrowsException<ArgumentNullException>(() => Base64.Encode(input));
        }

        [TestMethod]
        public void DecodeTest()
        {
            var expected = @"MyRandomString With<Some>'Unsafe'$Chars@";
            var input = @"TXlSYW5kb21TdHJpbmcgV2l0aDxTb21lPidVbnNhZmUnJENoYXJzQA==";
            var res = Base64.Decode(input);
            Assert.AreEqual(expected, res);
        }

        [TestMethod]
        public void Decode_notBase64Test()
        {
            var input = "random notBase64 string";
            Debug.WriteLine(input + " is not valid Base64 string");
            Assert.ThrowsException<FormatException>(() => Base64.Decode(input));
        }

        [TestMethod]
        public void Decode_nullTest()
        {
            string input = null;
            Assert.ThrowsException<ArgumentNullException>(() => Base64.Decode(input));
        }
    }
}