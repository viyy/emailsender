﻿using System.Windows;
using System.Windows.Input;

namespace EmailSender.Windows
{
    /// <summary>
    ///     Логика взаимодействия для WndMain.xaml
    /// </summary>
    public partial class WndMain : Window
    {
        public WndMain()
        {
            InitializeComponent();
        }

        //Pure UI Logic
        private void MoveWindow(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void HandlerProxy(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        private void ShedulerOn(object sender, RoutedEventArgs e)
        {
            ShedulerOverlay.Visibility = Visibility.Visible;
        }

        private void ShedulerOff(object sender, RoutedEventArgs e)
        {
            ShedulerOverlay.Visibility = Visibility.Collapsed;
        }

        private void AccountManagerOn(object sender, RoutedEventArgs e)
        {
            AccountOverlay.Visibility = Visibility.Visible;
        }

        private void ReceiversOverlayOn(object sender, RoutedEventArgs e)
        {
            ReceiversOverlay.Visibility = Visibility.Visible;
        }

        private void AccountManagerOff(object sender, RoutedEventArgs e)
        {
            AccountOverlay.Visibility = Visibility.Collapsed;
        }

        private void ReceiversOverlayOff(object sender, RoutedEventArgs e)
        {
            ReceiversOverlay.Visibility = Visibility.Collapsed;
        }

        //Old code
        /*     
        private void AccountsList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (AccountsList.SelectedIndex == -1) return;
            //var num = AccountsList.SelectedIndex;
            //var acc = DbManager.CurrentDb.Accounts.ElementAt(num);
            if (!(AccountsList.SelectedItem is Accounts acc)) return;
            UserLoginBox.Text = acc.Login;
            UserPasswordBox.Password =
                Nelfias.Crypto.Encrypt.DecryptString(Nelfias.Crypto.Base64.Decode(acc.Password),
                    Conf.Security.PassPhrase);
            AddressBox.Text = acc.Email;
            ServerBox.Text = acc.Smtp;
            PortBox.Text = acc.Port.ToString();
        }

        private void AddAccount(object sender, RoutedEventArgs e)
        {
            AccountsList.SelectedIndex = -1;
            UserLoginBox.Text = "";
            UserPasswordBox.Password = "";
            AddressBox.Text = "";
            ServerBox.Text = "";
            PortBox.Text = "";
        }

        private void RemoveAccount(object sender, RoutedEventArgs e)
        {
            //TODO: add question
            if (AccountsList.SelectedIndex == -1) return;
            //var acc = DbManager.CurrentDb.Accounts.ElementAt(AccountsList.SelectedIndex);
            if (AccountsList.SelectedItem is Accounts acc) DbManager.CurrentDb.RemoveAccount(acc.Id);
            AccountsList.ItemsSource = null;
            AccountsList.ItemsSource = DbManager.CurrentDb.Accounts;
        }

        private void SaveAccount(object sender, RoutedEventArgs e)
        {
            if (UserLoginBox.Text=="") return;
            if (UserPasswordBox.Password == "") return;
            if (AddressBox.Text=="") return;
            if (ServerBox.Text=="") return;
            if (PortBox.Text == "") return;
            if (!int.TryParse(PortBox.Text, out var port)) return;
            //TODO: add mask check;
            if (AccountsList.SelectedIndex == -1)
            {
                DbManager.CurrentDb.AddAccount(new Accounts
                {
                    Email = AddressBox.Text,
                    Login = UserLoginBox.Text,
                    Password = Nelfias.Crypto.Base64.Encode(Nelfias.Crypto.Encrypt.EncryptString(UserPasswordBox.Password,Conf.Security.PassPhrase)),
                    Smtp = ServerBox.Text,
                    Port = port
                });
            }
            else
            {
                if (AccountsList.SelectedItem is Accounts acc)
                {
                    acc.Email = AddressBox.Text;
                    acc.Login = UserLoginBox.Text;
                    acc.Password =
                        Nelfias.Crypto.Base64.Encode(
                            Nelfias.Crypto.Encrypt.EncryptString(UserPasswordBox.Password, Conf.Security.PassPhrase));
                    acc.Smtp = ServerBox.Text;
                    acc.Port = port;
                    DbManager.CurrentDb.UpdateAccount(acc);
                }
            }
            AccountsList.ItemsSource = null;
            AccountsList.ItemsSource = DbManager.CurrentDb.Accounts;
        }



        private void CloseApp(object sender, RoutedEventArgs e)
        {
            DbManager.CurrentDb.SaveChanges();
            Close();
        }



        public ObservableCollection<Receivers> AllReceivers { get; private set; } = new ObservableCollection<Receivers>();
        public ObservableCollection<Receivers> CurrentReceivers { get; private set; } = new ObservableCollection<Receivers>();
        private void AddList(object sender, RoutedEventArgs e)
        {
            ListsView.SelectedIndex = -1;
            ListNameBox.Text = "";
            AllReceivers = new ObservableCollection<Receivers>(DbManager.CurrentDb.Receivers);
            CurrentReceivers = new ObservableCollection<Receivers>();
        }

        private void RemoveList(object sender, RoutedEventArgs e)
        {
            if (ListsView.SelectedIndex == -1) return;
            if (ListsView.SelectedItem is Lists l) DbManager.CurrentDb.RemoveList(l.Id);
            ListsView.ItemsSource = null;
            ListsView.ItemsSource = DbManager.CurrentDb.Lists;
        }

        private void MoveReceiverToList(object sender, RoutedEventArgs e)
        {
            var idsToDel = new List<int>();
            foreach (var selectedItem in AllReceiversView.SelectedItems)
            {
                /*var num = AllReceiversView.Items.IndexOf(selectedItem);
                CurrentReceivers.Add(AllReceivers[num]);
                idsToDel.Add(AllReceivers[num].Id);
                if (!(selectedItem is Receivers rec)) continue;
                CurrentReceivers.Add(rec);
                idsToDel.Add(rec.Id);
            }
            foreach (var i in idsToDel)
            {
                AllReceivers.Remove(AllReceivers.First(t => t.Id == i));
            }
        }

        private void MoveReceiverFromList(object sender, RoutedEventArgs e)
        {
            var idsToDel = new List<int>();
            foreach (var selectedItem in CurrentListReceiversView.SelectedItems)
            {
                //var num = CurrentListReceiversView.Items.IndexOf(selectedItem);
                //AllReceivers.Add(CurrentReceivers[num]);
                //idsToDel.Add(CurrentReceivers[num].Id);
                if (!(selectedItem is Receivers rec)) continue;
                AllReceivers.Add(rec);
                idsToDel.Add(rec.Id);
            }
            foreach (var i in idsToDel)
            {
                CurrentReceivers.Remove(CurrentReceivers.First(t => t.Id == i));
            }
        }

        

        private void SaveList(object sender, RoutedEventArgs e)
        {
            //TODO error handle
            if (ListNameBox.Text == "") return;
            if (ListsView.SelectedIndex == -1)
            {
                var l = new Lists
                {
                    Name = ListNameBox.Text
                };
                DbManager.CurrentDb.AddList(l);
                foreach (var receiver in CurrentReceivers)
                {
                    DbManager.CurrentDb.AddReceiverToList(l.Id,receiver.Id);
                }
                DbManager.CurrentDb.SaveChanges();
                ListsView.ItemsSource = null;
                ListsView.ItemsSource = DbManager.CurrentDb.Lists;
                return;
            }
            if (ListsView.SelectedItem is Lists li)
            {
                li.Name = ListNameBox.Text;
                DbManager.CurrentDb.UpdateList(li);
                foreach (var receiver in CurrentReceivers)
                {
                    if (li.ListReceivers.Any(r => r.ReceiverId == receiver.Id)) continue;
                    DbManager.CurrentDb.AddReceiverToList(li.Id, receiver.Id);
                }
                DbManager.CurrentDb.SaveChanges();
            }
            li = DbManager.CurrentDb.Lists.ElementAt(ListsView.SelectedIndex);
            foreach (var listReceiver in li.ListReceivers)
            {
                if (CurrentReceivers.Any(r=>r.Id==listReceiver.ReceiverId)) continue;
                DbManager.CurrentDb.RemoveReceiverFromList(li.Id,listReceiver.ReceiverId);
            }
            ListsView.ItemsSource = null;
            ListsView.ItemsSource = DbManager.CurrentDb.Lists;
        }

        private void AddReceiver(object sender, RoutedEventArgs e)
        {
            AllReceiversListView.SelectedIndex = -1;
            ReceiverEmailBox.Text = "";
            ReceiverNameBox.Text = "";
        }

        private void RemoveReceiver(object sender, RoutedEventArgs e)
        {
            if (AllReceiversListView.SelectedIndex==-1) return;
            if (AllReceiversListView.SelectedItem is Receivers rec)
                DbManager.CurrentDb.RemoveReceiver(rec.Id);
            AllReceiversListView.ItemsSource = null;
            AllReceiversListView.ItemsSource = DbManager.CurrentDb.Receivers;
        }

        private void ListsView_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(ListsView.SelectedIndex==-1) return;
            if (ListsView.SelectedItem is Lists li)
            {
                ListNameBox.Text = li.Name;
                CurrentReceivers = new ObservableCollection<Receivers>(
                    DbManager.CurrentDb.Receivers.Where(r => li.ListReceivers.Any(lr => lr.ReceiverId == r.Id)));
                AllReceivers = new ObservableCollection<Receivers>(
                    DbManager.CurrentDb.Receivers.Where(r => li.ListReceivers.All(lr => lr.ReceiverId != r.Id)));
            }
            CurrentListReceiversView.ItemsSource = CurrentReceivers;
            AllReceiversView.ItemsSource = AllReceivers;
        }

        private void AllReceiversListView_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (AllReceiversListView.SelectedIndex == -1) return;
            if (!(AllReceiversListView.SelectedItem is Receivers rec)) return;
            ReceiverEmailBox.Text = rec.Email;
            ReceiverNameBox.Text = rec.Name;
        }

        private void SaveReceiver(object sender, RoutedEventArgs e)
        {
            //TODO: Error handle
            if (ReceiverEmailBox.Text == "" || ReceiverNameBox.Text == "") return;
            if (AllReceiversListView.SelectedIndex == -1)
            {
                //TODO: check values
                DbManager.CurrentDb.AddReceiver(new Receivers
                {
                    Name = ReceiverNameBox.Text,
                    Email = ReceiverEmailBox.Text
                });
                AllReceiversListView.ItemsSource = null;
                AllReceiversListView.ItemsSource = DbManager.CurrentDb.Receivers;
                return;
            }
            var rec = DbManager.CurrentDb.Receivers.ElementAt(AllReceiversListView.SelectedIndex);
            rec.Name = ReceiverNameBox.Text;
            rec.Email = ReceiverEmailBox.Text;
            DbManager.CurrentDb.UpdateReceiver(rec);
            AllReceiversListView.ItemsSource = null;
            AllReceiversListView.ItemsSource = DbManager.CurrentDb.Receivers;
        }

        private void SendNow(object sender, RoutedEventArgs e)
        {
            
                if (MainRtb.Text == "")
                {
                    //TODO: inWindow overlay
                    new WndNotify(NotificationTypes.Error, "Пустое сообщение", "Напишите текст сообщения",
                            ButtonTypes.Ok)
                        .ShowDialog();
                    return;
                }
                if (AccountSelectorBox.SelectedIndex == -1)
                {
                    new WndNotify(NotificationTypes.Error, "Не выбран аккаунт",
                            "Выберите учетную запись, с которой будете отправлять сообщения.", ButtonTypes.Ok)
                        .ShowDialog();
                    return;
                }
                var acc = AccountSelectorBox.SelectedItem as Accounts;
                var snd = new SmtpEmailSender();
            try
            {
                if (acc != null && !snd.Auth(acc.Smtp, acc.Port,
                        new NetworkCredential(acc.Login,
                            Nelfias.Crypto.Encrypt.DecryptString(Nelfias.Crypto.Base64.Decode(acc.Password),
                                Conf.Security.PassPhrase))))
                {
                    new WndNotify(NotificationTypes.Error, "Ошибка авторизации",
                            $"Не удалось подключиться к {acc.Smtp}:{acc.Port}", ButtonTypes.Ok)
                        .ShowDialog();
                    return;
                }
                if (ReceiverTypeTabControl.SelectedIndex == 0)
                {
                    foreach (var item in ListsSelectorView.SelectedItems)
                    {
                        //var n = ListsSelectorView.Items.IndexOf(item);
                        var l = item as Lists;
                        var recs = DbManager.CurrentDb.Receivers.Where(
                            r => { return l != null && l.ListReceivers.Any(lr => lr.ReceiverId == r.Id); });
                        if (acc != null)
                            snd.SendBatch(acc.Email, recs.Select(r => r.Email).ToList(), SubjectBox.Text,
                                RtfToHtmlConverter.ConvertRtfToHtml(MainRtb.Text));
                    }
                }
                else
                {
                    foreach (var item in ReceiversSelectorView.SelectedItems)
                    {
                        if (acc == null) continue;
                        if (item is Receivers rec)
                            snd.Send(acc.Email, rec.Email, SubjectBox.Text,
                                RtfToHtmlConverter.ConvertRtfToHtml(MainRtb.Text));
                    }
                }

            }
            catch (Exception ex)
            {

                new WndNotify(NotificationTypes.Error, "Ошибка", ex.Message, ButtonTypes.Ok).ShowDialog();
                snd.Dispose();
                return;
            }
            snd.Dispose();
            new WndNotify(NotificationTypes.Success, "Отправлено", "Все сообщения успешно отправлены", ButtonTypes.Ok)
                .ShowDialog();
            
        }

        

        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!(e.Source is TabControl)) return;
            ListsView.ItemsSource = null;
            ListsView.ItemsSource = DbManager.CurrentDb.Lists;
            AllReceiversListView.ItemsSource = null;
            AllReceiversListView.ItemsSource = DbManager.CurrentDb.Receivers;
        }*/
    }
}