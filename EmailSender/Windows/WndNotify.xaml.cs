﻿using System;
using System.Windows;
using System.Windows.Input;
using EmailSender.Context;

namespace EmailSender.Windows
{
    /// <summary>
    ///     Логика взаимодействия для WndNotify.xaml
    /// </summary>
    public partial class WndNotify : Window
    {
        public WndNotify(NotificationTypes msgType, string msgTitle, string msgText, ButtonTypes buttons)
        {
            NotificationType = msgType;
            Text = msgText;
            InitializeComponent();
            Title = msgTitle;
            OkButton = (buttons & ButtonTypes.Ok) != 0;
            CancelButton = (buttons & ButtonTypes.Cancel) != 0;
            YesButton = (buttons & ButtonTypes.Yes) != 0;
            NoButton = (buttons & ButtonTypes.No) != 0;
        }

        public bool OkButton { get; }
        public bool CancelButton { get; }
        public bool YesButton { get; }
        public bool NoButton { get; }
        public NotificationTypes NotificationType { get; private set; }
        public string Text { get; private set; }
        public event EventHandler MessageResult;

        protected virtual void OnMessageResult(MessageResultArgs e)
        {
            MessageResult?.Invoke(this, e);
        }

        private void CloseWnd(object sender, RoutedEventArgs e)
        {
            OnMessageResult(new MessageResultArgs {ButtonClicked = ButtonTypes.Cancel});
            Close();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Rectangle_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        private void OkBtnClick(object sender, RoutedEventArgs e)
        {
            OnMessageResult(new MessageResultArgs {ButtonClicked = ButtonTypes.Ok});
            Close();
        }

        private void YesBtnClick(object sender, RoutedEventArgs e)
        {
            OnMessageResult(new MessageResultArgs {ButtonClicked = ButtonTypes.Yes});
            Close();
        }

        private void NoBtnClick(object sender, RoutedEventArgs e)
        {
            OnMessageResult(new MessageResultArgs {ButtonClicked = ButtonTypes.No});
            Close();
        }
    }
}