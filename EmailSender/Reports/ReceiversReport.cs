﻿using System;
using System.Linq;
using EmailSender.Interfaces;
using Microsoft.Win32;
using Xceed.Words.NET;

namespace EmailSender.Reports
{
    public class ReceiversReport : IReport
    {
        public void Generate(IDataAccessLayer dataProvider)
        {
            var dlg = new SaveFileDialog
            {
                Filter = "Docx|*.docx",
                DefaultExt = "docx",
                AddExtension = true
            };
            if (dlg.ShowDialog()!=true) return;
            using (var doc = DocX.Create(dlg.FileName))
            {
                doc.InsertParagraph("Current Lists:").FontSize(18d).SpacingAfter(10d);
                var recs = dataProvider.GetReceivers();
                foreach (var list in dataProvider.GetLists())
                {
                    var p = doc.InsertParagraph($"List: {list.Name}").FontSize(14d).SpacingAfter(5d).SpacingBefore(5);
                    var t = doc.AddTable(list.ListReceivers.Count, 2);
                    for (var i = 0; i < list.ListReceivers.Count; i++)
                    {
                        var lr = list.ListReceivers[i];
                        var rc = recs.FirstOrDefault(r => r.Id == lr.ReceiverId);
                        if (rc==null) continue;
                        t.Rows[i].Cells[0].Paragraphs[0].Append(rc.Email);
                        t.Rows[i].Cells[1].Paragraphs[0].Append(rc.Name);
                    }
                    p.InsertTableAfterSelf(t);
                }

                doc.Save();
            }
        }
    }
}