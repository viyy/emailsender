﻿using Nelfias.Crypto;

namespace EmailSender.Context
{
    public static class Conf
    {
        public static class Db
        {
            public const string ConnectionString =
                @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=EmailSenderDb;Integrated Security=True";
        }

        public static class Security
        {
            private static readonly string PassPhrase = Base64.Decode("Z0IybmpuVjgyZlFUS0ZqaA==");

            public static string EncodePassword(string password)
            {
                return Base64.Encode(Encrypt.EncryptString(password, PassPhrase));
            }

            public static string DecodePassword(string password)
            {
                return Encrypt.DecryptString(Base64.Decode(password), PassPhrase);
            }
        }
    }
}