﻿using EmailSender.Context.Import;
using EmailSender.Interfaces;

namespace EmailSender.Context
{
    public static class ImportFactory
    {
        public static IListImporter GetImporter(string fromFormat)
        {
            switch (fromFormat)
            {
                case "csv":
                    return new CsvImport();
                case "xlsx":
                    return new ExcelImport();
                default:
                    return null;
            }
        }
    }
}