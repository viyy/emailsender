﻿using System;

namespace EmailSender.Context
{
    public class MessageResultArgs : EventArgs
    {
        public ButtonTypes ButtonClicked { get; set; }
    }
}