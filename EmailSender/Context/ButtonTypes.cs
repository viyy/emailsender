﻿using System;

namespace EmailSender.Context
{
    [Flags]
    public enum ButtonTypes
    {
        None = 0,
        Ok = 1,
        Cancel = 2,
        Yes = 4,
        No = 8
    }
}