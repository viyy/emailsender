﻿namespace EmailSender.Context
{
    public enum NotificationTypes
    {
        Error,
        Info,
        Warning,
        Success,
        Confirmation
    }
}