﻿using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using EmailSender.Context.DataModels;
using EmailSender.Interfaces;
using ExcelDataReader;
using Microsoft.Win32;
using Nelfias.Net.Validators;

namespace EmailSender.Context.Import
{
    public class CsvImport : IListImporter
    {
        public IEnumerable<Receivers> GetReceivers()
        {
            var dlg = new OpenFileDialog
            {
                CheckFileExists = true,
                Filter = "CSV table (*.csv)|*.csv",
                Multiselect = false
            };
            var res = dlg.ShowDialog();
            if (res != true) return new List<Receivers>();
            var list = new List<Receivers>();
            var regex = EmailValidator.GetRegex();
            using (var stream = File.Open(dlg.FileName, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateCsvReader(stream))
                {
                    var result = reader.AsDataSet().Tables[0];
                    list.AddRange(from DataRow row in result.Rows
                        where regex.IsMatch(row.ItemArray[0].ToString())
                        select new Receivers
                        {
                            Email = row.ItemArray[0].ToString(),
                            Name = row.ItemArray[1].ToString()
                        });
                }
            }
            return list;
        }
    }
}