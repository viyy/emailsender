﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using EmailSender.Interfaces;

namespace EmailSender.Context.DataModels
{
    public class DbManager : IDataAccessLayer
    {
        private readonly DbDataContext _db = new DbDataContext(Conf.Db.ConnectionString);

        public void SaveChanges()
        {
            _db.SubmitChanges();
        }

        #region Accounts

        public ObservableCollection<Accounts> GetAccounts()
        {
            return new ObservableCollection<Accounts>(_db.Accounts.Select(accounts => accounts));
        }

        public int AddAccount(Accounts acc)
        {
            _db.Accounts.InsertOnSubmit(acc);
            SaveChanges();
            return acc.Id;
        }

        public void RemoveAccount(int id)
        {
            var acc = _db.Accounts.SingleOrDefault(a => a.Id == id);
            if (acc == null) return;
            _db.Accounts.DeleteOnSubmit(acc);
            SaveChanges();
        }

        public int UpdateAccount(Accounts acc)
        {
            if (acc.Id == 0)
                return AddAccount(acc);
            var dacc = _db.Accounts.SingleOrDefault(a => a.Id == acc.Id);
            if (dacc == null)
            {
                acc.Id = 0;
                return AddAccount(acc);
            }
            dacc.Login = acc.Login;
            dacc.Password = acc.Password;
            dacc.Email = acc.Email;
            dacc.Smtp = acc.Smtp;
            dacc.Port = acc.Port;
            SaveChanges();
            return dacc.Id;
        }

        #endregion

        #region Lists

        public ObservableCollection<Lists> GetLists()
        {
            return new ObservableCollection<Lists>(_db.Lists.Select(lists => lists));
        }

        public Lists GetList(int id)
        {
            return _db.Lists.FirstOrDefault(l => l.Id == id);
        }

        public int AddList(Lists list)
        {
            _db.Lists.InsertOnSubmit(list);
            SaveChanges();
            return list.Id;
        }

        public void RemoveList(int id)
        {
            var l = _db.Lists.SingleOrDefault(t => t.Id == id);
            if (l == null) return;
            var r = _db.ListReceivers.Where(tr => tr.ListId == id);
            _db.ListReceivers.DeleteAllOnSubmit(r);
            _db.Lists.DeleteOnSubmit(l);
            SaveChanges();
        }

        public int UpdateList(Lists list)
        {
            var l = _db.Lists.SingleOrDefault(t => t.Id == list.Id);
            if (l == null)
                return AddList(list);
            l.Name = list.Name;
            SaveChanges();
            return l.Id;
        }

        #endregion

        #region Receivers

        public ObservableCollection<Receivers> GetReceivers()
        {
            return new ObservableCollection<Receivers>(_db.Receivers.Select(r => r));
        }

        public int AddReceiver(Receivers receiver)
        {
            _db.Receivers.InsertOnSubmit(receiver);
            SaveChanges();
            return receiver.Id;
        }

        public void RemoveReceiver(int id)
        {
            var rec = _db.Receivers.SingleOrDefault(r => r.Id == id);
            if (rec == null) return;
            var lr = _db.ListReceivers.Where(r => r.ReceiverId == id);
            _db.ListReceivers.DeleteAllOnSubmit(lr);
            var er = _db.EmailReceivers.Where(r => r.ReceiverId == id);
            _db.EmailReceivers.DeleteAllOnSubmit(er);
            _db.Receivers.DeleteOnSubmit(rec);
            SaveChanges();
        }

        public int UpdateReceiver(Receivers receiver)
        {
            var rec = _db.Receivers.SingleOrDefault(r => r.Id == receiver.Id);
            if (rec == null)
                return AddReceiver(receiver);
            rec.Email = receiver.Email;
            rec.Name = receiver.Name;
            SaveChanges();
            return rec.Id;
        }

        #endregion

        #region ListReceivers

        public ObservableCollection<ListReceivers> GetListReceivers()
        {
            return new ObservableCollection<ListReceivers>(_db.ListReceivers.Select(t => t));
        }

        public ObservableCollection<EmailReceivers> GetEmailReceivers()
        {
            return new ObservableCollection<EmailReceivers>(_db.EmailReceivers.Select(t => t));
        }

        public void AddReceiverToList(int listId, int receiverId)
        {
            if (_db.ListReceivers.SingleOrDefault(i => i.ListId == listId && i.ReceiverId == receiverId) !=
                null) return;
            _db.ListReceivers.InsertOnSubmit(new ListReceivers
            {
                ListId = listId,
                ReceiverId = receiverId
            });
            SaveChanges();
        }

        public void RemoveReceiverFromList(int listId, int receiverId)
        {
            var t = _db.ListReceivers.SingleOrDefault(i => i.ListId == listId && i.ReceiverId == receiverId);
            if (t == null) return;
            _db.ListReceivers.DeleteOnSubmit(t);
            SaveChanges();
        }

        public ObservableCollection<Receivers> GetListReceivers(int listId)
        {
            var lr = _db.ListReceivers.Where(r => r.ListId == listId);
            return new ObservableCollection<Receivers>(_db.Receivers.Where(r => lr.Any(t => t.ReceiverId == r.Id)));
        }

        #endregion

        #region Emails

        public ObservableCollection<Emails> GetEmails()
        {
            return new ObservableCollection<Emails>(_db.Emails.Select(t => t));
        }

        public int AddEmail(Emails email)
        {
            _db.Emails.InsertOnSubmit(email);
            SaveChanges();
            return email.Id;
        }

        public int UpdateEmail(Emails email)
        {
            if (email.Id == 0)
                return AddEmail(email);
            var de = _db.Emails.SingleOrDefault(e => e.Id == email.Id);
            if (de == null) return AddEmail(email);
            de = email;
            SaveChanges();
            return de.Id;
        }

        public void RemoveEmail(int id)
        {
            var de = _db.Emails.SingleOrDefault(e => e.Id == id);
            if (de == null) return;
            _db.Emails.DeleteOnSubmit(de);
            SaveChanges();
        }

        #endregion

        #region EmailReceivers

        public ObservableCollection<EmailReceivers> GetEmailReceivers(int emailId)
        {
            return new ObservableCollection<EmailReceivers>(_db.EmailReceivers.Where(e => e.EmailId == emailId));
        }

        public void AddEmailReceiver(int emailId, int receiverId)
        {
            _db.EmailReceivers.InsertOnSubmit(new EmailReceivers
            {
                EmailId = emailId,
                ReceiverId = receiverId
            });
            SaveChanges();
        }

        public void AddEmailReceivers(int emailId, IEnumerable<int> receiverIds)
        {
            foreach (var receiverId in receiverIds)
                _db.EmailReceivers.InsertOnSubmit(new EmailReceivers
                {
                    EmailId = emailId,
                    ReceiverId = receiverId
                });
            SaveChanges();
        }

        public void RemoveEmailReceivers(int emailId)
        {
            var recs = _db.EmailReceivers.Where(r => r.EmailId == emailId);
            _db.EmailReceivers.DeleteAllOnSubmit(recs);
            SaveChanges();
        }

        public void RemoveEmailReceiver(int emailId, int receiverId)
        {
            var rec = _db.EmailReceivers.SingleOrDefault(r => r.EmailId == emailId && r.ReceiverId == receiverId);
            if (rec != null) _db.EmailReceivers.DeleteOnSubmit(rec);
            SaveChanges();
        }

        #endregion
    }
}