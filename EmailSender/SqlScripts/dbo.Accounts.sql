﻿CREATE TABLE [dbo].[Accounts] (
    [Id]       INT           IDENTITY (1, 1) NOT NULL,
    [Login]    NVARCHAR (50) NOT NULL,
    [Password] NVARCHAR (50) NOT NULL,
    [Email]    NVARCHAR (50) NOT NULL,
    [Smtp]     NVARCHAR (50) NOT NULL,
    [Port]     INT           NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

