﻿CREATE TABLE [dbo].[Lists] (
    [Id]   INT           IDENTITY (1, 1) NOT NULL,
    [Name] NVARCHAR (50) DEFAULT ('new list') NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

