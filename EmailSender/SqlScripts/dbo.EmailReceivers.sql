﻿CREATE TABLE [dbo].[EmailReceivers] (
    [EmailId]    INT NOT NULL,
    [ReceiverId] INT NOT NULL,
    CONSTRAINT [EmailReceivers_PK] PRIMARY KEY CLUSTERED ([EmailId] ASC, [ReceiverId] ASC)
);

