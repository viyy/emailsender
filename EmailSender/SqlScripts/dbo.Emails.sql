﻿CREATE TABLE [dbo].[Emails] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [AccountId] INT           NOT NULL,
    [Subject]   NVARCHAR (50) DEFAULT ('no subject') NOT NULL,
    [Body]      TEXT          DEFAULT ('') NOT NULL,
    [SendTime]  DATETIME      NOT NULL,
    [Sended]    BIT           DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

