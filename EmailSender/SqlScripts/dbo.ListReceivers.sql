﻿CREATE TABLE [dbo].[ListReceivers] (
    [ListId]     INT NOT NULL,
    [ReceiverId] INT NOT NULL,
    CONSTRAINT [ListReceivers_PK] PRIMARY KEY CLUSTERED ([ListId] ASC, [ReceiverId] ASC)
);

