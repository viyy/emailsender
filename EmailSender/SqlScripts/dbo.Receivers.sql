﻿CREATE TABLE [dbo].[Receivers] (
    [Id]    INT           IDENTITY (1, 1) NOT NULL,
    [Email] NVARCHAR (50) NOT NULL,
    [Name]  NVARCHAR (50) DEFAULT ('') NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

