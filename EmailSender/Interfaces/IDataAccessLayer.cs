﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using EmailSender.Context.DataModels;

namespace EmailSender.Interfaces
{
    public interface IDataAccessLayer
    {
        //Tables
        ObservableCollection<Accounts> GetAccounts();

        ObservableCollection<Emails> GetEmails();
        ObservableCollection<Receivers> GetReceivers();
        ObservableCollection<Lists> GetLists();
        ObservableCollection<ListReceivers> GetListReceivers();

        ObservableCollection<EmailReceivers> GetEmailReceivers();

        //Get by Id
        Lists GetList(int id);

        //Utility
        void SaveChanges();

        //CRUD
        //Accounts
        int AddAccount(Accounts acc);

        void RemoveAccount(int id);

        int UpdateAccount(Accounts acc);

        //Lists
        int AddList(Lists list);

        void RemoveList(int id);

        int UpdateList(Lists list);

        //Receivers
        int AddReceiver(Receivers receiver);

        void RemoveReceiver(int id);

        int UpdateReceiver(Receivers receiver);

        //ListsReceivers
        void AddReceiverToList(int listId, int receiverId);

        void RemoveReceiverFromList(int listId, int receiverId);

        ObservableCollection<Receivers> GetListReceivers(int listId);

        //Emails
        int AddEmail(Emails email);

        int UpdateEmail(Emails email);
        void RemoveEmail(int id);

        //EmailReceivers
        void AddEmailReceiver(int emailId, int receiverId);

        void AddEmailReceivers(int emailId, IEnumerable<int> receiverIds);
        void RemoveEmailReceivers(int emailId);
        void RemoveEmailReceiver(int emailId, int receiverId);
    }
}