﻿namespace EmailSender.Interfaces
{
    public interface IReport
    {
        void Generate(IDataAccessLayer dataProvider);
    }
}