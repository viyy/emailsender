﻿using System.Collections.Generic;
using EmailSender.Context.DataModels;

namespace EmailSender.Interfaces
{
    public interface IListImporter
    {
        IEnumerable<Receivers> GetReceivers();
    }
}