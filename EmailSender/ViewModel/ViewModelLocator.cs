/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:EmailSender"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using CommonServiceLocator;
using EmailSender.Context.DataModels;
using EmailSender.Interfaces;

namespace EmailSender.ViewModel
{

    public class ViewModelLocator
    {

        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            SimpleIoc.Default.Register<WndMainViewModel>();
            SimpleIoc.Default.Register<IDataAccessLayer, DbManager>();
        }

        public WndMainViewModel WndMain
        {
            get
            {
                return ServiceLocator.Current.GetInstance<WndMainViewModel>();
            }
        }
        
        public static void Cleanup()
        {
        }
    }
}