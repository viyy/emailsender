using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using EmailSender.Context;
using EmailSender.Context.DataModels;
using EmailSender.Interfaces;
using EmailSender.Reports;
using EmailSender.Utils;
using EmailSender.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Nelfias.Net.Email;

namespace EmailSender.ViewModel
{
    public class WndMainViewModel : ViewModelBase
    {
        #region Constructor

        public WndMainViewModel(IDataAccessLayer dataProvider)
        {
            _dataProvider = dataProvider;
            GetAllCommand = new RelayCommand(GetAll);
            CreateNewAccountCommand = new RelayCommand(CreateNewAccount);
            AccountsListSelectCommand = new RelayCommand<PasswordBox>(AmSelectionChanged);
            PasswordChangedCommand = new RelayCommand<PasswordBox>(SetPassword);
            CloseAppCommand = new RelayCommand(() =>
            {
                SaveData();
                Application.Current.Shutdown();
            });
            RemoveAccountCommand =
                new RelayCommand(() => RemoveAccount(AmSelectedAccount), () => AmSelectedAccount != null);
            CreateNewListCommand = new RelayCommand(AddList);
            RemoveListCommand = new RelayCommand(() => RemoveList(LmCurrentList), () => LmCurrentList != null);
            MoveToListCommand = new RelayCommand<IEnumerable<object>>(MoveToList, CanMoveToList);
            MoveFromListCommand = new RelayCommand<IEnumerable<object>>(MoveFromList, CanMoveToList);
            CreateNewReceiverCommand = new RelayCommand(CreateReceiver);
            RemoveReceiverCommand =
                new RelayCommand(() => RemoveReceiver(RmCurrentReceiver), () => RmCurrentReceiver != null);
            ListSelectionChangedCommand = new RelayCommand<IEnumerable<object>>(SetListSelection);
            ReceiversSelectionChangedCommand = new RelayCommand<IEnumerable<object>>(SetReceiversSelection);
            SendNowCommand = new RelayCommand(SendNow);
            SheduleCommand = new RelayCommand(SheduleEmail);
            DeleteEmailCommand = new RelayCommand(RemoveSheduledEmail, () => ShedulerSelectedEmail != null);
            ImportCommand = new RelayCommand<string>(ImportReceivers, o => ImportList != null);
            ReportCommand = new RelayCommand(()=>{new ReceiversReport().Generate(_dataProvider);});
        }

        #endregion

        #region Tables

        private readonly IDataAccessLayer _dataProvider;

        private ObservableCollection<Accounts> _accounts = new ObservableCollection<Accounts>();

        public ObservableCollection<Accounts> Accounts
        {
            get => _accounts;
            set => Set(ref _accounts, value);
        }

        public ObservableCollection<Lists> Lists
        {
            get => _lists;
            set => Set(ref _lists, value);
        }

        private ObservableCollection<Lists> _lists = new ObservableCollection<Lists>();

        private ObservableCollection<Emails> _emails = new ObservableCollection<Emails>();

        public ObservableCollection<Emails> Emails
        {
            get => _emails;
            set => Set(ref _emails, value);
        }

        private ObservableCollection<Receivers> _receivers = new ObservableCollection<Receivers>();

        public ObservableCollection<Receivers> Receivers
        {
            get => _receivers;
            set => Set(ref _receivers, value);
        }

        public ObservableCollection<ListReceivers> ListReceivers
        {
            get => _listReceivers;
            set => Set(ref _listReceivers, value);
        }

        public ObservableCollection<EmailReceivers> EmailReceivers
        {
            get => _emailReceivers;
            set => Set(ref _emailReceivers, value);
        }

        private ObservableCollection<ListReceivers> _listReceivers = new ObservableCollection<ListReceivers>();

        private ObservableCollection<EmailReceivers> _emailReceivers = new ObservableCollection<EmailReceivers>();

        #endregion Tables

        #region Commands

        //Common
        public ICommand GetAllCommand { get; }

        public ICommand CloseAppCommand { get; }

        public ICommand SendNowCommand { get; }

        public ICommand ReportCommand { get; set; }

        //Account
        public ICommand CreateNewAccountCommand { get; }

        public ICommand RemoveAccountCommand { get; }
        public RelayCommand<PasswordBox> AccountsListSelectCommand { get; }
        public RelayCommand<PasswordBox> PasswordChangedCommand { get; }

        //Lists
        public ICommand CreateNewListCommand { get; }

        public ICommand RemoveListCommand { get; }
        public RelayCommand<IEnumerable<object>> MoveToListCommand { get; }
        public RelayCommand<IEnumerable<object>> MoveFromListCommand { get; }

        public RelayCommand<IEnumerable<object>> ListSelectionChangedCommand { get; }

        //Receivers
        public ICommand CreateNewReceiverCommand { get; }

        public ICommand RemoveReceiverCommand { get; }

        public RelayCommand<IEnumerable<object>> ReceiversSelectionChangedCommand { get; }

        public RelayCommand<string> ImportCommand { get; }

        //Emails
        public ICommand SheduleCommand { get; }

        public ICommand DeleteEmailCommand { get; }

        #endregion

        #region Email

        //send
        private string _subject;

        public string Subject
        {
            get => _subject;
            set => Set(ref _subject, value);
        }

        private string _body;

        public string Body
        {
            get => _body;
            set => Set(ref _body, value);
        }

        private void SendNow()
        {
            if (_body == "")
            {
                //TODO: inWindow overlay
                new WndNotify(NotificationTypes.Error, "������ ���������", "�������� ����� ���������",
                        ButtonTypes.Ok)
                    .ShowDialog();
                return;
            }
            if (CurrentAccount == null)
            {
                new WndNotify(NotificationTypes.Error, "�� ������ �������",
                        "�������� ������� ������, � ������� ������ ���������� ���������.", ButtonTypes.Ok)
                    .ShowDialog();
                return;
            }
            var acc = CurrentAccount;
            var snd = new SmtpEmailSender();
            try
            {
                if (!snd.Auth(acc.Smtp, acc.Port,
                    new NetworkCredential(acc.Login, Conf.Security.DecodePassword(acc.Password))))
                {
                    new WndNotify(NotificationTypes.Error, "������ �����������",
                            $"�� ������� ������������ � {acc.Smtp}:{acc.Port}", ButtonTypes.Ok)
                        .ShowDialog();
                    return;
                }
                if (SelectedTab == 0)
                    foreach (var item in _currentSelectedLists)
                    {
                        var recs = Receivers.Where(
                            r => { return item.ListReceivers.Any(lr => lr.ReceiverId == r.Id); });
                        snd.SendBatch(acc.Email, recs.Select(r => r.Email).ToList(), Subject,
                            RtfToHtmlConverter.ConvertRtfToHtml(Body));
                    }
                else
                {
                    _currentSelectedReceivers.AsParallel().ForAll(item =>
                    {
                        if (item is Receivers rec)
                            // ReSharper disable once AccessToDisposedClosure
                            snd?.Send(acc.Email, rec.Email, Subject,
                                RtfToHtmlConverter.ConvertRtfToHtml(Body));
                    });
                    /*foreach (var item in _currentSelectedReceivers)
                    {
                        if (acc == null) throw new NullReferenceException("Account isn't selected");
                        if (item is Receivers rec)
                            snd.Send(acc.Email, rec.Email, Subject,
                                RtfToHtmlConverter.ConvertRtfToHtml(Body));
                    }*/
                }
            }
            catch (Exception ex)
            {
                new WndNotify(NotificationTypes.Error, "������", ex.Message, ButtonTypes.Ok).ShowDialog();
                snd.Dispose();
                return;
            }
            finally
            {
                snd.Dispose();
            }
            new WndNotify(NotificationTypes.Success, "����������", "��� ��������� ������� ����������", ButtonTypes.Ok)
                .ShowDialog();
        }

        //work
        private ObservableCollection<Emails> _plannedEmails;

        public ObservableCollection<Emails> PlannedEmails
        {
            get => _plannedEmails;
            set => Set(ref _plannedEmails, value);
        }

        private Emails _shedulerSelectedEmail;

        public Emails ShedulerSelectedEmail
        {
            get => _shedulerSelectedEmail;
            set => Set(ref _shedulerSelectedEmail, value);
        }

        private DateTime _plannedDate;

        public DateTime PlannedDate
        {
            get => _plannedDate;
            set => Set(ref _plannedDate, value);
        }

        private DateTime _plannedTime;

        public DateTime PlannedTime
        {
            get => _plannedTime;
            set => Set(ref _plannedTime, value);
        }

        private void SheduleEmail()
        {
            var email = new Emails
            {
                Subject = Subject,
                AccountId = CurrentAccount.Id,
                Body = Body,
                Sended = false,
                SendTime = new DateTime(PlannedDate.Year, PlannedDate.Month, PlannedDate.Day, PlannedTime.Hour,
                    PlannedTime.Minute, 0)
            };
            email.Id = _dataProvider.AddEmail(email);
            Emails = _dataProvider.GetEmails();
            PlannedEmails = new ObservableCollection<Emails>(Emails.Where(e => e.Sended == false));
            RaisePropertyChanged(nameof(PlannedEmails));
            if (SelectedTab == 0)
                foreach (var item in _currentSelectedLists)
                {
                    //var n = ListsSelectorView.Items.IndexOf(item);;
                    var recs = Receivers.Where(
                        r => { return item.ListReceivers.Any(lr => lr.ReceiverId == r.Id); }).ToList();
                    _dataProvider.AddEmailReceivers(email.Id, recs.Select(r => r.Id));
                }
            else
                foreach (var item in _currentSelectedReceivers)
                {
                    if (item is Receivers rec)
                        _dataProvider.AddEmailReceiver(email.Id, rec.Id);
                }
            EmailReceivers = _dataProvider.GetEmailReceivers();
            RaisePropertyChanged(nameof(EmailReceivers));
        }

        private void RemoveSheduledEmail()
        {
            _dataProvider.RemoveEmailReceivers(_shedulerSelectedEmail.Id);
            _dataProvider.RemoveEmail(_shedulerSelectedEmail.Id);
            ShedulerSelectedEmail = null;
            EmailReceivers = _dataProvider.GetEmailReceivers();
            Emails = _dataProvider.GetEmails();
            RaisePropertyChanged(nameof(Emails));
            RaisePropertyChanged(nameof(EmailReceivers));
            PlannedEmails = new ObservableCollection<Emails>(Emails.Where(e => e.Sended == false));
            RaisePropertyChanged(nameof(PlannedEmails));
        }

        #endregion

        #region Utility

        private int _selectedTab;

        public int SelectedTab
        {
            get => _selectedTab;
            set => Set(ref _selectedTab, value);
        }

        private void SaveData()
        {
            foreach (var account in _accounts)
                _dataProvider.UpdateAccount(account);
            foreach (var list in Lists)
                _dataProvider.UpdateList(list);
        }

        private void GetAll()
        {
            Accounts = _dataProvider.GetAccounts();
            Lists = _dataProvider.GetLists();
            Receivers = _dataProvider.GetReceivers();
            ListReceivers = _dataProvider.GetListReceivers();
            Emails = _dataProvider.GetEmails();
            EmailReceivers = _dataProvider.GetEmailReceivers();
            PlannedEmails = new ObservableCollection<Emails>(Emails.Where(e => e.Sended == false));
        }

        #endregion

        #region AccountManager 

        //Account Manager
        private Accounts _currentAccount;

        public Accounts CurrentAccount
        {
            get => _currentAccount;
            set => Set(ref _currentAccount, value);
        }

        private Accounts _amSelectedAccount;

        public Accounts AmSelectedAccount
        {
            get => _amSelectedAccount;
            set => Set(ref _amSelectedAccount, value);
        }

        private void AmSelectionChanged(PasswordBox box)
        {
            if (_amSelectedAccount == null) return;
            box.Password = Conf.Security.DecodePassword(_amSelectedAccount.Password);
        }

        private void CreateNewAccount()
        {
            var acc = new Accounts
            {
                Login = "<Your Login>",
                Password = Conf.Security.EncodePassword(""),
                Email = "<New account>",
                Smtp = "smtp.example.com",
                Port = 587
            };
            acc.Id = _dataProvider.AddAccount(acc);
            Accounts = _dataProvider.GetAccounts();
            AmSelectedAccount = acc;
        }

        private void SetPassword(PasswordBox box)
        {
            AmSelectedAccount.Password = Conf.Security.EncodePassword(box.Password);
        }

        private void RemoveAccount(Accounts acc)
        {
            _dataProvider.RemoveAccount(acc.Id);
            AmSelectedAccount = null;
            Accounts = _dataProvider.GetAccounts();
        }

        #endregion

        #region Lists

        private Lists _importList;

        public Lists ImportList
        {
            get => _importList;
            set => Set(ref _importList, value);
        }

        private List<Lists> _currentSelectedLists = new List<Lists>();

        private void SetListSelection(IEnumerable<object> lists)
        {
            _currentSelectedLists = new List<Lists>();
            foreach (var list in lists)
            {
                if (!(list is Lists l)) return;
                _currentSelectedLists.Add(l);
            }
        }

        private Lists _lmCurrentList;

        public Lists LmCurrentList
        {
            get => _lmCurrentList;
            set
            {
                Set(ref _lmCurrentList, value);
                if (_lmCurrentList == null) return;
                LmCurrentReceivers = _dataProvider.GetListReceivers(_lmCurrentList.Id);
                LmUnselectedReceivers = new ObservableCollection<Receivers>(Receivers.Except(LmCurrentReceivers));
                RaisePropertyChanged(nameof(LmCurrentReceivers));
                RaisePropertyChanged(nameof(LmUnselectedReceivers));
            }
        }

        private ObservableCollection<Receivers> _lmCurrentReceivers;

        public ObservableCollection<Receivers> LmCurrentReceivers
        {
            get => _lmCurrentReceivers;
            set => Set(ref _lmCurrentReceivers, value);
        }

        private ObservableCollection<Receivers> _lmUnselectedReceivers;

        public ObservableCollection<Receivers> LmUnselectedReceivers
        {
            get => _lmUnselectedReceivers;
            set => Set(ref _lmUnselectedReceivers, value);
        }

        private void AddList()
        {
            var list = new Lists
            {
                Name = "<New List>"
            };
            list.Id = _dataProvider.AddList(list);
            Lists = _dataProvider.GetLists();
            LmCurrentList = list;
        }

        private void RemoveList(Lists list)
        {
            _dataProvider.RemoveList(list.Id);
            LmCurrentList = null;
            Lists = _dataProvider.GetLists();
            LmCurrentReceivers = new ObservableCollection<Receivers>();
            LmUnselectedReceivers = new ObservableCollection<Receivers>();
            RaisePropertyChanged(nameof(LmCurrentReceivers));
            RaisePropertyChanged(nameof(LmUnselectedReceivers));
        }

        private static bool CanMoveToList(IEnumerable<object> recs)
        {
            return recs != null && recs.Any();
        }

        private void MoveToList(IEnumerable<object> recs)
        {
            foreach (var rec in recs)
            {
                if (!(rec is Receivers r)) return;
                _dataProvider.AddReceiverToList(LmCurrentList.Id, r.Id);
            }
            LmCurrentList = _dataProvider.GetList(LmCurrentList.Id);
        }

        private void MoveFromList(IEnumerable<object> recs)
        {
            foreach (var rec in recs)
            {
                if (!(rec is Receivers r)) return;
                _dataProvider.RemoveReceiverFromList(LmCurrentList.Id, r.Id);
            }
            LmCurrentList = _dataProvider.GetList(LmCurrentList.Id);
        }

        #endregion

        #region Receivers

        private List<Receivers> _currentSelectedReceivers = new List<Receivers>();

        private void SetReceiversSelection(IEnumerable<object> recs)
        {
            _currentSelectedReceivers = new List<Receivers>();
            foreach (var rec in recs)
            {
                if (!(rec is Receivers r)) return;
                _currentSelectedReceivers.Add(r);
            }
        }

        private Receivers _rmCurrentReceiver;

        public Receivers RmCurrentReceiver
        {
            get => _rmCurrentReceiver;
            set => Set(ref _rmCurrentReceiver, value);
        }

        private void CreateReceiver()
        {
            var rec = new Receivers
            {
                Email = "<E-mail>",
                Name = "<Name>"
            };
            rec.Id = _dataProvider.AddReceiver(rec);
            Receivers = _dataProvider.GetReceivers();
            RmCurrentReceiver = rec;
        }

        private void RemoveReceiver(Receivers rec)
        {
            _dataProvider.RemoveReceiver(rec.Id);
            Receivers = _dataProvider.GetReceivers();
            RmCurrentReceiver = null;
        }

        private void ImportReceivers(string fromFormat)
        {
            var recs = ImportFactory.GetImporter(fromFormat)?.GetReceivers().ToList();
            if (recs == null)
            {
                new WndNotify(NotificationTypes.Error, "������ ��������", "������������ �������� ������� ��� �������",
                    ButtonTypes.Ok).ShowDialog();
                return;
            }
            if (!recs.Any()) return;
            foreach (var rec in recs)
            {
                rec.Id = _dataProvider.AddReceiver(rec);
                _dataProvider.AddReceiverToList(ImportList.Id, rec.Id);
            }
            Lists = _dataProvider.GetLists();
            Receivers = _dataProvider.GetReceivers();
            ListReceivers = _dataProvider.GetListReceivers();
            new WndNotify(NotificationTypes.Success, "������", "������ ������� ��������", ButtonTypes.Ok).ShowDialog();
        }

        #endregion
    }
}