﻿using System.Globalization;
using System.Windows.Controls;

namespace EmailSender.ValidationRules
{
    public class PortNumberValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (!(value is string portStr)) return new ValidationResult(false, "Некорректные данные");
            if (!int.TryParse(portStr, out var port))
                return new ValidationResult(false, "Номер порта может быть только числом");
            return port > 0 && port < 65536
                ? ValidationResult.ValidResult
                : new ValidationResult(false, "Порт должен быть в пределах [1; 65535]");
        }
    }
}