﻿using System.Globalization;
using System.Windows.Controls;
using Nelfias.Net.Validators;

namespace EmailSender.ValidationRules
{
    public class EmailRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (!(value is string email)) return new ValidationResult(false, "Некорректные данные");
            return EmailValidator.GetRegex().IsMatch(email)
                ? ValidationResult.ValidResult
                : new ValidationResult(false, "Некорректный адрес");
        }
    }
}