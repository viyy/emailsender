﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Nelfias.Crypto;

namespace EmailSender.Utils
{
    [XmlRoot("UserData")]
    [Obsolete]
    public class UserData
    {
        public string Login { get; set; }

        public string Base64Password { get; set; }

        [XmlIgnore]
        public string Password
        {
            get => Base64.Decode(Base64Password);
            set => Base64Password = Base64.Encode(value);
        }

        public string Email { get; set; }
        public string Server { get; set; }
        public string Port { get; set; }

        public void Save(string path)
        {
            var serializer = new XmlSerializer(typeof(UserData));
            using (Stream fileStream = new FileStream(path, FileMode.Create, FileAccess.ReadWrite))
            {
                using (XmlWriter xmlWriter = new XmlTextWriter(fileStream, Encoding.UTF8))
                {
                    var ns = new XmlSerializerNamespaces();
                    ns.Add("", "");
                    serializer.Serialize(xmlWriter, this, ns);
                }
            }
        }

        public static UserData Load(string path)
        {
            UserData t;
            if (!File.Exists(path)) return null;
            var deserializer = new XmlSerializer(typeof(UserData));
            using (TextReader reader = new StreamReader(path))
            {
                t = deserializer.Deserialize(reader) as UserData;
                if (t == null) return null;
            }
            return t;
        }
    }
}