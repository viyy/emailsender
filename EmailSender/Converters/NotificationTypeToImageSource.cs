﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;
using EmailSender.Context;

namespace EmailSender.Converters
{
    public class NotificationTypeToImageSource : IValueConverter
    {
        private readonly Dictionary<NotificationTypes, string> _icons = new Dictionary<NotificationTypes, string>
        {
            {NotificationTypes.Info, "../Assets/Icons/png/004-notification.png"},
            {NotificationTypes.Warning, "../Assets/Icons/png/001-warning.png"},
            {NotificationTypes.Success, "../Assets/Icons/png/002-checked.png"},
            {NotificationTypes.Error, "../Assets/Icons/png/003-cancel.png"},
            {NotificationTypes.Confirmation, "../Assets/Icons/png/005-doubt.png"}
        };

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && _icons.ContainsKey((NotificationTypes) value))
                return new Uri(_icons[(NotificationTypes) value], UriKind.Relative);

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return NotificationTypes.Info;
        }
    }
}