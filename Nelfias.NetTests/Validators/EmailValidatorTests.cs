﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Nelfias.Net.Validators.Tests
{
    [TestClass]
    public class EmailValidatorTests
    {
        [TestMethod]
        public void GetRegexTest()
        {
            var valid = new List<string>
            {
                "email@example.com",
                "firstname.lastname@example.com",
                "email@subdomain.example.com",
                "firstname+lastname@example.com",
                "\"email\"@example.com",
                "1234567890@example.com",
                "email@example-one.com",
                "_______@example.com",
                "email@example.name",
                "email@example.museum",
                "email@example.co.jp",
                "firstname-lastname@example.com",
                "あいうえお@example.com"
            };
            foreach (var email in valid)
                Assert.IsTrue(EmailValidator.GetRegex().IsMatch(email), $"{email} didn't pass");
        }

        [TestMethod]
        public void GetRegex_InvalidTest()
        {
            var invalid = new List<string>
            {
                "plainaddress",
                "#@%^%#$@#$@#.com",
                "@example.com",
                "Joe Smith <email@example.com>",
                "email.example.com",
                "email@example@example.com",
                ".email@example.com",
                "email.@example.com",
                "email..email@example.com",
                "email@example.com (Joe Smith)",
                "email@example",
                "email@-example.com",
                "email@111.222.333.44444",
                "email@example..com",
                "Abc..123@example.com"
            };
            foreach (var email in invalid)
                Assert.IsFalse(EmailValidator.GetRegex().IsMatch(email), $"{email} didn't pass");
        }
    }
}