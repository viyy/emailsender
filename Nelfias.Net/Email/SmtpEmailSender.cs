﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using Nelfias.Net.Interfaces;

namespace Nelfias.Net.Email
{
    public class SmtpEmailSender : IMessageSender, IDisposable
    {
        private SmtpClient _client;

        public void Dispose()
        {
            _client.Dispose();
        }

        public bool Auth(string server, int port, NetworkCredential userCredential)
        {
            _client = new SmtpClient(server, port)
            {
                UseDefaultCredentials = false,
                Credentials = userCredential,
                EnableSsl = true,
                Timeout = 10000,
                DeliveryMethod = SmtpDeliveryMethod.Network
            };
            return true;
        }

        public bool Send(string from, string to, string subject, string msg)
        {
            var mail = new MailMessage(from, to, subject, msg)
            {
                IsBodyHtml = true,
                BodyEncoding = Encoding.UTF8
            };
            _client.Send(mail);
            mail.Dispose();
            return true;
        }

        public bool SendBatch(string from, List<string> toList, string subject, string msg)
        {
            toList.Distinct().AsParallel().ForAll(to =>
            {
                var mail = new MailMessage(from, to, subject, msg)
                {
                    IsBodyHtml = true,
                    BodyEncoding = Encoding.UTF8
                };
                _client.Send(mail);
                mail.Dispose();
            });
            return true;
        }
    }
}