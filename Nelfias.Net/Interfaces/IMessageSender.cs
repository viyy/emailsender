﻿using System.Collections.Generic;
using System.Net;

namespace Nelfias.Net.Interfaces
{
    public interface IMessageSender
    {
        bool Auth(string server, int port, NetworkCredential userCredential);
        bool Send(string from, string to, string subject, string msg);
        bool SendBatch(string from, List<string> toList, string subject, string msg);
    }
}